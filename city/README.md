### tf-serving
#### `utils/tf_serving`
* 'utils/tf_serving/model_jpeg.py': use jpeg input, output ckpt for making saved model;
* 'utils/tf_serving/ckpt2savedModel.py`: convert ckpt to saved model 

* run tf serving
sudo docker run -t --rm -p 8500:8500 -v "/home/jiachao/JCLiu/detection/detection_mf_2020/city/utils/tf_serving/savedmodel_city:/models/city" -e MODEL_NAME=city --gpus device=GPU-942729fe-de33-939b-bf9d-fd2145f15aa9 tensorflow/serving:latest-gpu

