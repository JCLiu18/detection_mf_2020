import os
import json
import argparse
import cv2
import numpy as np
from  random import shuffle

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--out_jsn', default='/ssd/data/data_jc/city_test/annotations/instances_train.json')
arg_parser.add_argument('--inp_jsns', default='/ssd/data/data_jc/city_test/annotations/instances_train.json')
args = arg_parser.parse_args()

#coco format
#y.keys(): dict_keys(['licenses', 'info', 'annotations', 'categories', 'images'])
#'licenses': [{'url':str, 'id':int, 'name':str}]
#'info': {'description':str, 'url':str, 'version':int, 'year':int, 'contrbutor': str, 'data_create':str}
#'categories':[{'supercategory':'city_regu', 'id':1, 'name':'exposure'}]
#'annotations': [{'segmentation', 'bbox', 'id', 'category_id', 'area', 'image_id', 'iscrowd'}]
#'images': [{'coco_url', 'height', 'file_name', 'width', 'license', 'date_captured', 'id', 'flickr_url'}]

def parser(jsn_list, out_jsn, cls_list, name_map):
    cnt_dict = dict()
    for cls in cls_list:
        cnt_dict[cls] = 0
        
    out = dict()

    out['categories'] = []
    for i, cls in enumerate(cls_list):
        out['categories'].append({'supercategory':'{}'.format(cls), 'id':i+1, 'name':'{}'.format(cls)})
    
    out['annotations'] = list()
    out['images'] = list()

    bbox_cnt = 0 
    img_cnt = 0
    no_bbox = 0
    err = 0
    for jsn in jsn_list:
        print('load json: ', jsn)
        dataset = json.load(open(jsn, 'r'))
        img_root= jsn.replace('.json', '')
        print(img_root)

        for index, img in enumerate(sorted(os.listdir(img_root))):
            img_fp = os.path.join(img_root, img)
            img_info = dict()
            img_info['file_name'] = img_fp 
            try:
                IMAGE = cv2.imread(img_fp)
                img_info['height'], img_info['width'] = IMAGE.shape[0], IMAGE.shape[1]
            except:
                if not img_fp.endswith('json'):
                    print('{} not exits'.format(img_fp))
                continue

            try:
                info = dataset['frames'][img]
            except:
                print('{} has no annotation'.format(img))
                err += 1
                continue

            #ann_info = dict()
            if len(info) == 0:
                print('no bbox in img {}!!'.format(img))
                no_bbox += 1
                #continue

            for bbox in info:
                ann_info = dict()
                ann_info['segmentation'] = []
                ann_info['area'] = 100.0 
                ann_info['iscrowd'] = 0 
                ann_info['bbox'] = []
                if len(bbox['tags'])==0:
                    print('no tag in img {}!!'.format(img))
                    continue
                
                if bbox['tags'][0] not in name_map.keys():
                    continue

                if name_map[bbox['tags'][0]] in cls_list:

                    ann_info['category_id'] = cls_list.index(bbox['tags'][0]) + 1 
                    cnt_dict[bbox['tags'][0]] +=1
                else:
                    print(bbox['tags'])
                    print('*** no relevant tag in a box of img {}!!'.format(img))
                    continue

                ann_info['image_id'] = img_cnt 
                ann_info['id'] = bbox_cnt 

                if bbox_cnt%1000 == 0 and bbox_cnt != 0:
                    print('processed {} bboxs'.format(bbox_cnt))
                bbox_cnt += 1

                ann_info['bbox'] = [bbox['x1'],bbox['y1'],bbox['x2']-bbox['x1'],bbox['y2']-bbox['y1']]
                out['annotations'].append(ann_info)

            img_info['id'] = img_cnt 
            out['images'].append(img_info)

            if img_cnt%1000 == 0 and img_cnt != 0:
                print('processed {} imgs'.format(img_cnt))
            img_cnt += 1

    print('\n**** statistics ****')
    print('err: {}'.format(err))
    print('processed {} imgs in all dirs'.format(img_cnt))
    print('processed {} bboxs in all dirs'.format(bbox_cnt))
    print("cls: {}".format(out['categories']))
    for k, v in cnt_dict.items():
        print('{}: {} bboxs'.format(k, v))
    with open(out_jsn, 'w') as fpr:
        json.dump(out, fpr, indent=2)
        print('** {}  preparation is done! **'.format(out_jsn))


if __name__ == '__main__':
    jsn_list=args.inp_jsns.split(',')
    #cls_list = ['person', 'nonvehicle', 'vehicle', 'face']
    #name_map = dict(person='person', nonvehicle='nonvehicle', vehicle='vehicle', face='face')
    cls_list = ['peddler', 'occupy', 'exposure', 'billboard', 'hanger', 'noparking', 'cover', 'trashbag', 'stack', 'trashcan']
    name_map = dict(peddler='peddler', occupy='occupy', exposure='exposure', billboard='billboard', hanger='hanger', noparking='noparking', cover='cover', trashbag='trashbag', stack='stack', trashcan='trashcan')
    print(args.out_jsn)
    out_fp = args.out_jsn.replace(args.out_jsn.split('/')[-1], '')
    print(out_fp)
    if not os.path.exists(out_fp) and out_fp != None:
        os.makedirs(out_fp)
    parser(jsn_list, args.out_jsn, cls_list, name_map)


