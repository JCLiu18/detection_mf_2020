import os
import argparse


def parse_args():
    parser = argparse.ArgumentParser(description='inference detection')
    parser.add_argument('--input_format', help='input format', default='jpeg', type=str)
    parser.add_argument('--classesList', help='classes  list',
                        default=['peddler', 'occupy', 'exposure', 'billboard', 'hanger', 'noparking', 'cover', 'trashbag', 'trashcan', 'stack'], type=list)
    parser.add_argument('--network', help='network', default='resnet50_v2', type=str)
    parser.add_argument('--GPU', help='GPU', default='3', type=str)
    parser.add_argument('--ckpt_fp', type=str) 
    parser.add_argument('--img_fp', type=str) 
    parser.add_argument('--savePath', help='pb path', type=str)
    parser.add_argument('--topk_bbox', help='topk bbox', type=int, default=10)
    args = parser.parse_args()
    return args
