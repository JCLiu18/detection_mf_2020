import os
import cv2
from utils.ctdet import LoadModel
from config import parse_args
from utils.tf_slide_img import transformImage
import numpy as np


def predict(args):
    model = LoadModel(ckptPath=args.ckptPath, args=args)
    img_fps = list()
    for root, dirs, files in os.walk(args.testImagesPath):
        for f in files:
            if f.endswith('jpg') or f.endswith('png'):
                img_fps.append(os.path.join(root, f))

    for index, imagePath in enumerate(img_fps):
        image = cv2.imread(imagePath)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        ori_image = image

        h, w, _ = image.shape
        off_h, off_w = int(h*0.05), int(w*0.05)
        image = image[args.off_h:(h-args.off_h), args.off_w:(w-args.off_w)]
        scale = args.img_size / min(h, w)
        
        dim = (int(w * scale), int(h * scale))
        image = cv2.resize(image, dim, interpolation=cv2.INTER_LINEAR)
        name = imagePath.split('/')[-1]
        _ = model.detect(image, name, ori_image, scale)
        print('.', end='', flush = True)
    model.sort_cls_bbox()
    print('\n*** processed images saved at {} ***'.format(args.savePath))


if __name__ == '__main__':
    args = parse_args()
    os.environ['CUDA_VISIBLE_DEVICES'] = args.GPU
    predict(args)
