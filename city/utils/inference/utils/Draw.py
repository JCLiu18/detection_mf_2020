import colorsys
import cv2
import pickle
import numpy as np
import os

def _init():
    classes = ['peddler', 'occupy', 'exposure', 'billboard', 'hanger', 'noparking', 'cover', 'trashbag', 'stack', 'trashcan'] 
    thresholds = {
            'peddler':0.2, 
            'occupy':0.2, 
            'exposure':0.2, 
            'billboard':0.2, 
            'hanger':0.2, 
            'noparking':0.2, 
            'cover':0.2, 
            'trashbag':0.2,
            'trashcan':0.2, 
            'stack':0.2}
    colors = _colors(classes)
    return classes, thresholds, colors
    

def _colors(classes):
    num_classes = len(classes) 
    class_to_ind = dict(zip(classes, range(1, num_classes + 1)))
    hsv_tuples = [(1.0 * x / num_classes, 1., 1.) for x in range(num_classes)]
    color_list = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
    color_list = list(map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), color_list))
    colors = dict(zip(classes, color_list))
    return colors 


def overlap(x1, w1, x2, w2):
    left = x1 if x1 > x2 else x2
    r1 = x1 + w1
    r2 = x2 + w2
    right = r1 if r1 < r2 else r2
    return max(right - left, 0)


def box_intersection(a, b):
    w = overlap(a[0], a[2] - a[0], b[0], b[2] - b[0])
    h = overlap(a[1], a[3] - a[1], b[1], b[3] - b[1])
    return w * h


def box_union(a, b):
    area_i = box_intersection(a, b)
    area_u = (a[2] - a[0]) * (a[3] - a[1]) + (b[2] - b[0]) * (b[3] - b[1]) - area_i
    return area_u


def box_iou(a, b):
    return box_intersection(a, b) / box_union(a, b)


def box_uoi(a, b):
    x1 = max(min(a[0], b[0]),0)
    y1 = max(min(a[1], b[1]),0)
    x2 = max(max(a[2], b[2]),0)
    y2 = max(max(a[3], b[3]),0)
    return [x1,y1,x2,y2, b[4], b[5]]


def show_results(image, dets, name, scale, savePath):
    classes, thresholds, colors = _init()
    cnt = 0

    for i, cls in enumerate(classes):
        dets_cls = dets[dets[:,-1]==i]
        dets_cls = dets_cls[dets_cls[:,-2]>=thresholds[cls]]
        scores = dets_cls[:,4]
        order = scores.argsort()[::-1]
        
        if dets_cls.size == 0:
            continue

        keep = [dets_cls[order[0]]]
        for det in dets_cls[order[1:]]:
            for j, det_keep in enumerate(keep):
                if box_iou(det, det_keep) > 0.2:
                    keep[j] = box_uoi(det, det_keep)
                else:
                    keep.append(det)

        for det in keep:
            bbox, score, cls = det[:4], det[4], int(det[5])
            xmin, ymin, xmax, ymax = int(bbox[0]/scale[1]), int(bbox[1]/scale[0]), int(bbox[2]/scale[1]), int(bbox[3]/scale[0])
            cls_name = classes[cls]
            color = colors[cls_name]  
            cv2.rectangle(image, (xmin, ymin), (xmax, ymax), color, 2)
            cv2.putText(image, cls_name, (xmin, ymin - 10), cv2.FONT_HERSHEY_COMPLEX, 2, color, 2)
            cnt += 1

    if cnt > 0:
        cv2.imwrite(os.path.join(savePath,name), image)


if __name__ == '__main__':
    ori_image, dets, name, scale = pickle.load(open('test.pkl','rb'))
    #show_results(ori_image, dets[0], name, scale)

