#python3 inference_test.py --GPU=0 --network=resnet50_v2_sparse --ckptPath=../checkpoints/exp02_city_resnet50_v2_AWsparse_img224 --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/ --savePath=./resultImage/exp02_city_resnet50_v2_AWsparse_img224_test0/ --img_size=224 --off_h 0 --off_w 0

#python3 inference.py --GPU=-1 --network=resnet50_v2_sparse --ckptPath=../checkpoints/exp04_city_resnet50_v2_AWsparse_img224_scale0p9_1p1 --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/ --slide=False --bbox_top=False --savePath=./resultImage/exp04_city_resnet50_v2_AWsparse_img224_scale0p9_1p1/ --img_size=224

#python3 inference.py --GPU=0 --network=resnet50_v2_sparse --ckptPath=../checkpoints/exp03_city_resnet50_v2_AWsparse_img320 --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/ --slide=False --bbox_top=False --savePath=./resultImage/exp03_city_resnet50_v2_AWsparse_img320/ --img_size=320

#python3 inference.py --GPU=0 --network=resnet50_v2_sparse --ckptPath=../checkpoints/exp02_city_resnet50_v2_AWsparse_img224 --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/ --slide=False --bbox_top=False --savePath=./resultImage/exp02_city_resnet50_v2_AWsparse_img224_test0/ --img_size=224

#python3 inference.py --GPU=3 --network=resnet50_v2_sparse --ckptPath=../checkpoints/city_resnet50_v2_sparse_img320 --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/ --slide=False --bbox_top=False --savePath=./resultImage/exp03_city_resnet50_v2_sparse_img320/ --img_size=320


#python3 inference.py  --GPU=0 --ckptPath=/ssd/chengzhang/code/project_city/detect_mf_1013/detect_mof-net_jc/checkpoints/prune09_resnet50_v2_32_512_256_block3_0.5_block4_0.8_50000_200_balance4/ --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/ --slide=True --bbox_top=False --savePath=./resultImage/prune09_resnet50_v2_32_512_256_slide49_city_test_img_1106/

#python3 inference.py  --GPU=1 --network=resnet50_v2_sparse --ckptPath=../checkpoints/city_resnet50_v2_sparse_img512 --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/ --slide=False --bbox_top=False --savePath=./resultImage/exp01_city_resnet50_v2_sparse_img512/

#python3 inference.py --GPU=3 --network=resnet50_v2_sparse --ckptPath=../checkpoints/city_resnet50_v2_sparse_img384 --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/ --slide=False --bbox_top=False --savePath=./resultImage/exp01_city_resnet50_v2_sparse_img384/ --img_size=384

#python3 inference_network.py --GPU=1 --network=resnet50_v2_sparse --ckptPath=../checkpoints/exp05_city_resnet50_v2_AWsparse_img224_1p2bbox --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/  --savePath=./resultImage/exp05_city_resnet50_v2_AWsparse_img224_1p2bbox/ --img_size=224

#python3 inference_network.py --GPU=2 --network=resnet50_v2_sparse --ckptPath=../checkpoints/exp05_city_resnet50_v2_AWsparse_img224_1p2bbox --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/  --savePath=./resultImage/test_city_resnet50_v2_AWsparse_img224_1p2bbox/ --img_size=224

python3 inference_network.py --GPU=2 --network=resnet50_v2_sparse --ckptPath=../checkpoints/exp07_city_resnet50_v2_AWsparse_img224_1p4bbox --testImagesPath=/ssd/submission/zhangcheng/20191106/inference_city/city_test_img_1106/  --savePath=./resultImage/exp07_city_resnet50_v2_AWsparse_img224_1p4bbox/ --img_size=224
