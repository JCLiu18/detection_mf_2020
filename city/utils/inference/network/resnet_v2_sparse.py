import tensorflow
import pandas
import sys
import os

mask_dict = pandas.read_pickle('network/mask_dict')

regularizer = tensorflow.contrib.layers.l1_l2_regularizer(1e-7, 1e-7)

def ResNet_v2_block(x, in_c, out_c, stride, training, num):
    y = tensorflow.layers.batch_normalization(
        inputs=x,
        axis=-1,
        momentum=0.99,
        epsilon=0.001,
        center=True,
        scale=True,
        beta_initializer=tensorflow.zeros_initializer(),
        gamma_initializer=tensorflow.ones_initializer(),
        moving_mean_initializer=tensorflow.zeros_initializer(),
        moving_variance_initializer=tensorflow.ones_initializer(),
        beta_regularizer=None,
        gamma_regularizer=None,
        beta_constraint=None,
        gamma_constraint=None,
        training=training,
        trainable=True,
        name=None,
        reuse=None,
        renorm=False,
        renorm_clipping=None,
        renorm_momentum=0.99,
        fused=None,
        virtual_batch_size=None,
        adjustment=None)
    y = tensorflow.nn.relu(
        features=y,
        name=None)

    if stride == 2 or x.shape.as_list()[3] != out_c:
        with tensorflow.variable_scope('conv2d_%s' % num):
            w1 = tensorflow.get_variable(
                name='kernel',
                shape=[1, 1, y.shape.as_list()[3], out_c],
                dtype=tensorflow.float32,
                initializer=tensorflow.initializers.glorot_uniform(),
                regularizer=regularizer)
            mask1 = tensorflow.get_variable(
                name='kernel/MomentumPrune_1',
                shape=[1, 1, y.shape.as_list()[3], out_c],
                dtype=tensorflow.float32,
                initializer=tensorflow.initializers.constant(mask_dict['conv2d_%s' % num]),
                trainable=False)
            w1 = w1 * mask1
            shortcut = tensorflow.nn.conv2d(
                input=y,
                filter=w1,
                strides=stride,
                padding='SAME',
                name=None)
            num = num + 1
    else:
        shortcut = x

    with tensorflow.variable_scope('conv2d_%s' % num):
        w2 = tensorflow.get_variable(
            name='kernel',
            shape=[1, 1, y.shape.as_list()[3], in_c],
            dtype=tensorflow.float32,
            initializer=tensorflow.initializers.glorot_uniform(),
            regularizer=regularizer)
        mask2 = tensorflow.get_variable(
            name='kernel/MomentumPrune_1',
            shape=[1, 1, y.shape.as_list()[3], in_c],
            dtype=tensorflow.float32,
            initializer=tensorflow.initializers.constant(mask_dict['conv2d_%s' % num]),
            trainable=False)
        w2 = w2 * mask2
        y = tensorflow.nn.conv2d(
            input=y,
            filter=w2,
            strides=1,
            padding='SAME',
            name=None)
        num = num + 1

    x = tensorflow.layers.batch_normalization(
        inputs=y,
        axis=-1,
        momentum=0.99,
        epsilon=0.001,
        center=True,
        scale=True,
        beta_initializer=tensorflow.zeros_initializer(),
        gamma_initializer=tensorflow.ones_initializer(),
        moving_mean_initializer=tensorflow.zeros_initializer(),
        moving_variance_initializer=tensorflow.ones_initializer(),
        beta_regularizer=None,
        gamma_regularizer=None,
        beta_constraint=None,
        gamma_constraint=None,
        training=training,
        trainable=True,
        name=None,
        reuse=None,
        renorm=False,
        renorm_clipping=None,
        renorm_momentum=0.99,
        fused=None,
        virtual_batch_size=None,
        adjustment=None)
    x = tensorflow.nn.relu(
        features=x,
        name=None)
    with tensorflow.variable_scope('conv2d_%s' % num):
        if stride > 1:
            x = tensorflow.pad(x, [[0, 0], [1, 1], [1, 1], [0, 0]])
        w3 = tensorflow.get_variable(
            name='kernel',
            shape=[3, 3, x.shape.as_list()[3], in_c],
            dtype=tensorflow.float32,
            initializer=tensorflow.initializers.glorot_uniform(),
            regularizer=regularizer)
        mask3 = tensorflow.get_variable(
            name='kernel/MomentumPrune_1',
            shape=[3, 3, x.shape.as_list()[3], in_c],
            dtype=tensorflow.float32,
            initializer=tensorflow.initializers.constant(mask_dict['conv2d_%s' % num]),
            trainable=False)
        w3 = w3 * mask3
        x = tensorflow.nn.conv2d(
            input=x,
            filter=w3,
            strides=stride,
            padding='SAME' if stride == 1 else 'VALID',
            name=None)
        num = num + 1

    x = tensorflow.layers.batch_normalization(
        inputs=x,
        axis=-1,
        momentum=0.99,
        epsilon=0.001,
        center=True,
        scale=True,
        beta_initializer=tensorflow.zeros_initializer(),
        gamma_initializer=tensorflow.ones_initializer(),
        moving_mean_initializer=tensorflow.zeros_initializer(),
        moving_variance_initializer=tensorflow.ones_initializer(),
        beta_regularizer=None,
        gamma_regularizer=None,
        beta_constraint=None,
        gamma_constraint=None,
        training=training,
        trainable=True,
        name=None,
        reuse=None,
        renorm=False,
        renorm_clipping=None,
        renorm_momentum=0.99,
        fused=None,
        virtual_batch_size=None,
        adjustment=None)
    x = tensorflow.nn.relu(
        features=x,
        name=None)
    with tensorflow.variable_scope('conv2d_%s' % num):
        w4 = tensorflow.get_variable(
            name='kernel',
            shape=[1, 1, x.shape.as_list()[3], out_c],
            dtype=tensorflow.float32,
            initializer=tensorflow.initializers.glorot_uniform(),
            regularizer=regularizer)
        mask4 = tensorflow.get_variable(
            name='kernel/MomentumPrune_1',
            shape=[1, 1, x.shape.as_list()[3], out_c],
            dtype=tensorflow.float32,
            initializer=tensorflow.initializers.constant(mask_dict['conv2d_%s' % num]),
            trainable=False)
        w4 = w4 * mask4
        x = tensorflow.nn.conv2d(
            input=x,
            filter=w4,
            strides=1,
            padding='SAME',
            name=None)
        x = shortcut + x
        num = num + 1
    return x, num

def ResNet_50(x, training):
    with tensorflow.variable_scope('student/resnet_model', reuse=tensorflow.AUTO_REUSE):
        with tensorflow.variable_scope('conv2d'):
            x = tensorflow.pad(x, [[0, 0], [3, 3], [3, 3], [0, 0]])
            w1 = tensorflow.get_variable(
                name='kernel',
                shape=[7, 7, 3, 64],
                dtype=tensorflow.float32,
                initializer=tensorflow.initializers.glorot_uniform(),
                regularizer=regularizer)
            mask1 = tensorflow.get_variable(
                name='kernel/MomentumPrune_1',
                shape=[7, 7, 3, 64],
                dtype=tensorflow.float32,
                initializer=tensorflow.initializers.constant(mask_dict['conv2d']),
                trainable=False)
            w1 = w1 * mask1
            x = tensorflow.nn.conv2d(
                input=x,
                filter=w1,
                strides=2,
                padding='VALID',
                name=None)
            x = tensorflow.layers.max_pooling2d(
                inputs=x,
                pool_size=3,
                strides=2,
                padding='same',
                data_format='channels_last',
                name=None)

        x, num = ResNet_v2_block(x, 64, 256, 1, training, 1)
        x, num = ResNet_v2_block(x, 64, 256, 1, training, num)
        x, num = ResNet_v2_block(x, 64, 256, 1, training, num)

        x, num = ResNet_v2_block(x, 128, 512, 2, training, num)
        x, num = ResNet_v2_block(x, 128, 512, 1, training, num)
        x, num = ResNet_v2_block(x, 128, 512, 1, training, num)
        x, num = ResNet_v2_block(x, 128, 512, 1, training, num)

        x, num = ResNet_v2_block(x, 256, 1024, 2, training, num)
        x, num = ResNet_v2_block(x, 256, 1024, 1, training, num)
        x, num = ResNet_v2_block(x, 256, 1024, 1, training, num)
        x, num = ResNet_v2_block(x, 256, 1024, 1, training, num)
        x, num = ResNet_v2_block(x, 256, 1024, 1, training, num)
        x, num = ResNet_v2_block(x, 256, 1024, 1, training, num)

        x, num = ResNet_v2_block(x, 512, 2048, 2, training, num)
        x, num = ResNet_v2_block(x, 512, 2048, 1, training, num)
        x, num = ResNet_v2_block(x, 512, 2048, 1, training, num)

        x = tensorflow.layers.batch_normalization(
            inputs=x,
            axis=-1,
            momentum=0.99,
            epsilon=0.001,
            center=True,
            scale=True,
            beta_initializer=tensorflow.zeros_initializer(),
            gamma_initializer=tensorflow.ones_initializer(),
            moving_mean_initializer=tensorflow.zeros_initializer(),
            moving_variance_initializer=tensorflow.ones_initializer(),
            beta_regularizer=None,
            gamma_regularizer=None,
            beta_constraint=None,
            gamma_constraint=None,
            training=training,
            trainable=True,
            name=None,
            reuse=None,
            renorm=False,
            renorm_clipping=None,
            renorm_momentum=0.99,
            fused=None,
            virtual_batch_size=None,
            adjustment=None)
        x = tensorflow.nn.relu(
            features=x,
            name=None)
        return x

if __name__ == '__main__':
    import os
    os.environ['CUDA_VISIBLE_DEVICES'] = '-1'

    # tensorflow.enable_eager_execution()

    x = tensorflow.random.uniform([1, 512, 512, 3], -1, 1)
    y = ResNet_50(x, False)
