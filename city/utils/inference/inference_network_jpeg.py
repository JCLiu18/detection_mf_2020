import os
import cv2 
import numpy as np
import tensorflow as tf
import utils.draw as draw
import copy
import pickle
from config import parse_args
from model_jpeg import LoadModel


def predict(args):
    model = LoadModel(args)

    if not os.path.exists(args.savePath):
        os.makedirs(args.savePath)

    img_fps = list()
    for f in os.listdir(args.img_fp):
        if f.endswith('jpg') or f.endswith('png'):
            img_fps.append(os.path.join(args.img_fp, f))

    for imagePath in img_fps:
        with open(imagePath, 'rb') as inp:
            img_jpeg = inp.read()
            print(imagePath)
            try:
                ori_image = cv2.imread(imagePath)
            except:
                print('cannot open {}'.format(imagePath))
                continue
            name = imagePath.split('/')[-1]
            dets = model.detect(img_jpeg)
            draw.show_results_jpeg(ori_image, dets[0], name, args.savePath)
            print('.', end='', flush=True)

    print('\n*** processed images saved at {} ***'.format(args.savePath))


if __name__ == '__main__':
    #os.environ['CUDA_VISIBLE_DEVICES'] = '-1' 
    args = parse_args()
    #args.ckpt_fp = '../../ckpts/exp01_city_resnet50_v2_img512_1p4bbox/'
    #args.ckpt_fp = '../../ckpts/exp03_city-27500img_resnet50_v2_img512_1p4bbox/'
    args.ckpt_fp = '../../ckpts/exp04_city-27500img_resnet50_v2_img512_1p4bbox_sparsity0.8/'
    args.img_fp = '../../data/test_data/city_raw_imgs'
    args.savePath = 'resultImages/city_raw_imgs_exp04_city-27500img_resnet50_v2_img512_1p4bbox_sparsity0.8'
    #args.img_fp = '../../data/test_data/test'
    #args.savePath = 'resultImages/test'
    args.topk_bbox = 5
    predict(args)
