import os
import cv2 
import numpy as np
import tensorflow as tf
import utils.Draw as draw
import copy
import pickle

def load_tf_graph(model_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()
    with open(model_file, "rb") as f:
        graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)
    return graph

def predict():
    #model_file = '../../ckpts/exp01_city_resnet50_v2_img512_1p4bbox/exp01_city_resnet50_v2_img512_1p4bbox.pb'
    model_file = '../../ckpts/exp01_city_resnet50_v2_img512_1p4bbox/exp01_city_resnet50_v2_img512_1p4bbox_img512_jpeg.pb'
    testImagesPath = '../../data/test_data/test_imgs_030220'
    savePath = 'resultImages/test_imgs_030220_jpeg'

    if not os.path.exists(savePath):
        os.makedirs(savePath)

    img_fps = list()
    for root, dirs, files in os.walk(testImagesPath):
        for f in files:
            if f.endswith('jpg') or f.endswith('png'):
                img_fps.append(os.path.join(root, f))

    graph = load_tf_graph(model_file)
    prediction = graph.get_tensor_by_name('import/detection:0')
    with tf.Session(graph=graph) as sess:
        for imagePath in img_fps:
            with open(imagePath, 'rb') as inp:
                img_jpeg = inp.read()
                image = cv2.imread(imagePath)
                h, w, _ = image.shape
                scale = [512.0/h, 512.0/w]
                name = imagePath.split('/')[-1]
                print('****', len(img_jpeg))
                dets = sess.run(prediction, feed_dict={'import/image:0':img_jpeg})
                draw.show_results(ori_image, dets[0], name, scale, savePath)
                print('.', end='', flush=True)

    print('\n*** processed images saved at {} ***'.format(savePath))


if __name__ == '__main__':
    #os.environ['CUDA_VISIBLE_DEVICES'] = '-1' 
    predict()
