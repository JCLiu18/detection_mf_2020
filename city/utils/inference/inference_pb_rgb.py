import os
import cv2 
import numpy as np
import tensorflow as tf
import utils.draw as draw
import copy
import pickle

def load_tf_graph(model_file):
    graph = tf.Graph()
    graph_def = tf.GraphDef()
    with open(model_file, "rb") as f:
        graph_def.ParseFromString(f.read())
    with graph.as_default():
        tf.import_graph_def(graph_def)
    return graph

def predict():
    model_file = '../../ckpts/exp01_city_resnet50_v2_img512_1p4bbox/exp01_city_resnet50_v2_img512_1p4bbox.pb'
    testImagesPath = '../../data/test_data/test_imgs_030420'
    savePath = 'resultImages/test_imgs_030420_224'
    img_size = 224 

    if not os.path.exists(savePath):
        os.makedirs(savePath)

    img_fps = list()
    for root, dirs, files in os.walk(testImagesPath):
        for f in files:
            if f.endswith('jpg') or f.endswith('png'):
                img_fps.append(os.path.join(root, f))

    graph = load_tf_graph(model_file)
    prediction = graph.get_tensor_by_name('import/detection:0')
    with tf.Session(graph=graph) as sess:
        for imagePath in img_fps:
            image = cv2.imread(imagePath)
            ori_image = copy.deepcopy(image)
            image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            h, w, _ = image.shape
            scale = [img_size/ min(h, w), img_size/ min(h, w)]
            dim = (int(w * scale[0]), int(h * scale[0]))
            image = cv2.resize(image, dim, interpolation=cv2.INTER_LINEAR)
            name = imagePath.split('/')[-1]
            dets = sess.run(prediction, feed_dict={'import/image:0':image})
            draw.show_results(ori_image, dets[0], name, scale, savePath)
            print('.', end='', flush=True)

    print('\n*** processed images saved at {} ***'.format(savePath))


if __name__ == '__main__':
    #os.environ['CUDA_VISIBLE_DEVICES'] = '-1' 
    predict()
