import grpc
import tensorflow as tf
import os
import cv2
import json
import numpy as np
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc
import draw
import copy


def run(img_fp, save_fp, server):
    channel = grpc.insecure_channel(server)
    stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)
    request = predict_pb2.PredictRequest()
    request.model_spec.name = 'city'
    request.model_spec.signature_name = 'serving_default'
    
    img_fps = list()
    for root, dirs, files in os.walk(img_fp):
        for f in files:
            if f.endswith('jpg') or f.endswith('png'):
                img_fps.append(os.path.join(root, f))


    for imagePath in img_fps:
        image = cv2.imread(imagePath)
        ori_image = copy.deepcopy(image)
        image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        h, w, _ = image.shape
        scale = [img_size/ min(h, w), img_size/ min(h, w)]
        dim = (int(w * scale[0]), int(h * scale[0]))
        image = cv2.resize(image, dim, interpolation=cv2.INTER_LINEAR)
        #scale = [512.0/h, 512.0/w]
        #image = cv2.resize(image, (512,512), interpolation=cv2.INTER_LINEAR)
        image = image.astype(np.float32)
        name = imagePath.split('/')[-1]

        request.inputs['input'].CopyFrom(
            #tf.contrib.util.make_tensor_proto(image, shape = [512,512,3]))
            tf.contrib.util.make_tensor_proto(image, shape = [dim[1],dim[0],3]))
        result = stub.Predict(request, 10.0)  
        dets = np.array(result.outputs['detection'].float_val, dtype = np.float32)
        dets = np.reshape(dets, (-1, 6))
        draw.show_results(ori_image, dets, name, scale, save_fp)
        print('.', end='', flush=True)

    print('\n*** processed images saved at {} ***'.format(save_fp))





if __name__ == '__main__':
    img_fp= '../../data/test_data/test_imgs_030220'
    save_fp= 'results/test_imgs_030220'
    server = 'localhost:8500'
    img_size = 512

    if not os.path.exists(save_fp):
        os.makedirs(save_fp)

    run(img_fp, save_fp, server)
