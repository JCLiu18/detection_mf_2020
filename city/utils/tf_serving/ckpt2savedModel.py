import tensorflow as tf
from tensorflow.python.training import saver as tf_saver
from tensorflow.python.framework import graph_util

export_dir = './savedmodel_city/0'
trained_checkpoint_prefix = 'tf_serving_ckpts/exp01_city_resnet50_v2_img224_1p4bbox'

graph = tf.Graph()
with tf.Session(graph=graph) as sess:
    ckpt = tf.train.latest_checkpoint(trained_checkpoint_prefix)
    saver = tf.compat.v1.train.import_meta_graph(ckpt + '.meta')
    saver.restore(sess, ckpt)

    builder = tf.saved_model.builder.SavedModelBuilder(export_dir)
    input_tensor = sess.graph.get_tensor_by_name('image:0')
    input_tensor = tf.saved_model.utils.build_tensor_info(input_tensor)
    output_detection_tensor = sess.graph.get_tensor_by_name('detection:0') 
    output_detection_tensor = tf.saved_model.utils.build_tensor_info(output_detection_tensor)
    prediction_signature = (
        tf.saved_model.signature_def_utils.build_signature_def(
            inputs={'input': input_tensor},
            outputs={'detection': output_detection_tensor},
            method_name=tf.saved_model.signature_constants.PREDICT_METHOD_NAME))
    builder.add_meta_graph_and_variables(
    sess, [tf.saved_model.tag_constants.SERVING],
    signature_def_map={
        tf.saved_model.signature_constants.DEFAULT_SERVING_SIGNATURE_DEF_KEY: prediction_signature
    },
    main_op=tf.tables_initializer(),
    strip_default_attrs=True)
    builder.save()
    print('*** saved model at {} ***'.format(export_dir))

