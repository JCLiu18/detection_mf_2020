import grpc
import tensorflow as tf
import os
import cv2
import json
import numpy as np
from tensorflow_serving.apis import predict_pb2
from tensorflow_serving.apis import prediction_service_pb2_grpc
import draw


def run(img_fp, save_fp, server):
    channel = grpc.insecure_channel(server)
    stub = prediction_service_pb2_grpc.PredictionServiceStub(channel)
    request = predict_pb2.PredictRequest()
    request.model_spec.name = 'city'
    request.model_spec.signature_name = 'serving_default'
    
    img_fps = list()
    for root, dirs, files in os.walk(img_fp):
        for f in files:
            if f.endswith('jpg') or f.endswith('png'):
                img_fps.append(os.path.join(root, f))


    for imagePath in img_fps:
        try:
            ori_image = cv2.imread(imagePath)
            name = imagePath.split('/')[-1]
        except:
            print('cannot open {}'.format(imagePath))
            continue

        with open(imagePath, 'rb') as inp:
            img_jpeg = inp.read()
        
        request.inputs['input'].CopyFrom(
            tf.contrib.util.make_tensor_proto(img_jpeg, shape = []))
        result = stub.Predict(request, 10.0)  
        dets = np.array(result.outputs['detection'].float_val, dtype = np.float32)
        dets = np.reshape(dets, (-1, 6))
        draw.show_results_jpeg(ori_image, dets, name, save_fp)
        print('.', end='', flush=True)

    print('\n*** processed images saved at {} ***'.format(save_fp))



if __name__ == '__main__':
    img_fp= '../../data/test_data/test_imgs_030920'
    save_fp= 'results/test_imgs_030920'
    server = 'localhost:8500'

    if not os.path.exists(save_fp):
        os.makedirs(save_fp)

    run(img_fp, save_fp, server)
