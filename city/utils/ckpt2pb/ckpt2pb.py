import tensorflow as tf
from tensorflow.python.training import saver as tf_saver
from tensorflow.python.framework import graph_util
from model_pb import LoadModel 
from config import parse_args

args = parse_args()
args.network = 'resnet50_v2'
#args.ckptPath = '../../ckpts/exp01_city_resnet50_v2_img512_1p4bbox'
args.ckptPath = '/home/jiachao/JCLiu/detection/detection_mf_2020/city/utils/tf_serving/tf_serving_ckpts/exp01_city_resnet50_v2_img224_1p4bbox'
args.savePath= 'pb_files/exp01_city_resnet50_v2_img224_1p4bbox.pb'
args.input = 'jpeg'

g = tf.Graph()
with g.as_default():
    model = LoadModel(args=args)
    input_graph_def = g.as_graph_def()
    saver = tf_saver.Saver()
    latest_checkpoint = tf.train.latest_checkpoint(args.ckptPath)
    sess = tf.Session()
    saver.restore(sess, latest_checkpoint)

    output_node_names = "detection"
    output_graph_def = graph_util.convert_variables_to_constants(
                        sess, # The session is used to retrieve the weights
                        input_graph_def, # The graph_def is used to retrieve the nodes 
                        output_node_names.split(",") # The output node names are used to select the usefull nodes
                                            )

    # Finally we serialize and dump the output graph to the filesystem
    with tf.gfile.GFile(args.savePath, "wb") as f:
        f.write(output_graph_def.SerializeToString())
        print('*** {} ***'.format(args.savePath))
