import tensorflow as tf
import numpy as np
import operator
import cv2
import operator
import os
import network.detector as detector 
from config import parse_args

args = parse_args()

def get_network(x, network, training=False):
    if network == 'resnet50_v2':
        import network.resnet_v2 as resnet_v2 
        x, _ = resnet_v2.ImagenetModel(resnet_size=50, data_format='channels_last')(x,training=training)
    elif network == 'resnet50_v2_sparse':
        import network.resnet_v2_sparse as resnet_v2_sparse 
        x = resnet_v2_sparse.ResNet_50(x, training)
    logit = detector.detect(x, training)
    return logit


class LoadModel(object):
    def __init__(self, args):
        mean = tf.constant([0.408, 0.447, 0.47], dtype=tf.float32)
        std = tf.constant([0.289, 0.274, 0.278], dtype=tf.float32)

        if args.input == 'rgb':
            self.image = tf.placeholder(tf.float32, [None, None, 3], 'image')
        elif args.input == 'jpeg':
            print('***')
            self.image_jpeg = tf.placeholder(tf.string, name = 'image')
            self.image = tf.image.decode_jpeg(self.image_jpeg)
            self.image = tf.image.resize(self.image, [512, 512], tf.image.ResizeMethod.BILINEAR)
        
        inp_image = (self.image/ 255. - mean) / std
        inp_image = tf.expand_dims(inp_image, 0)

        result = get_network(x=inp_image, network=args.network)

        heatmap = tf.sigmoid(result['heatmap'])
        hmax = tf.pad(heatmap, [[0, 0], [1, 1], [1, 1], [0, 0]], 'CONSTANT')
        hmax = tf.nn.max_pool2d(hmax, 3, 1, 'VALID')
        keep = tf.cast(tf.equal(hmax, heatmap), tf.float32)
        heatmap = heatmap * keep
        result['heatmap'] = heatmap

        self.result = self._decode(result['heatmap'], result['wh'], result['offset'], args.topk_bbox)


    def _tranpose_and_gather_feat(self, feat, ind):
        shape = tf.shape(feat)
        batch, height, width, cat = shape[0], shape[1], shape[2], shape[3]

        feat = tf.reshape(feat, [batch, -1, cat])
        feat = tf.gather(feat, ind, axis=1, batch_dims=1)
        return feat

    def _topk(self, scores, K):
        shape = tf.shape(scores)
        batch, height, width, cat = shape[0], shape[1], shape[2], shape[3]

        scores = tf.reshape(scores, [batch, -1])
        topk_score, indices = tf.math.top_k(scores, K)

        topk_clses = indices % cat
        topk_inds = (indices - topk_clses) // cat
        topk_ys = topk_inds // width
        topk_xs = topk_inds % width
        return topk_score, topk_inds, topk_clses, topk_ys, topk_xs

    def _decode(self, heat, wh, reg, K):
        scores, inds, clses, cy, cx = self._topk(heat, K)
        scores = tf.expand_dims(scores, -1)
        clses = tf.expand_dims(clses, -1)
        clses = tf.cast(clses, tf.float32)

        reg = self._tranpose_and_gather_feat(reg, inds)
        wh = self._tranpose_and_gather_feat(wh, inds)

        cy = tf.cast(cy, tf.float32)
        cx = tf.cast(cx, tf.float32)
        cy = cy + reg[:, :, 1]
        cx = cx + reg[:, :, 0]
        y1 = cy - wh[..., 1] / 2
        x1 = cx - wh[..., 0] / 2
        y2 = cy + wh[..., 1] / 2
        x2 = cx + wh[..., 0] / 2
        bboxes = tf.stack([x1, y1, x2, y2], -1) * 4
        detections = tf.concat([bboxes, scores, clses], -1, 'detection')
        return detections

