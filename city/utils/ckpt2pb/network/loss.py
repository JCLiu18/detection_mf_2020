import tensorflow as tf


def FocalLoss(heatmap, gt):
    pred = tf.nn.sigmoid(heatmap)
    pred = tf.clip_by_value(pred, 1e-4, 1 - 1e-4)

    pos_inds = tf.cast(tf.math.equal(gt, 1), tf.float32)
    neg_inds = tf.cast(tf.math.less(gt, 1), tf.float32)
    neg_weights = tf.math.pow(1 - gt, 4)

    pos_loss = tf.math.log(pred) * tf.math.pow(1 - pred, 2) * pos_inds
    neg_loss = tf.math.log(1 - pred) * tf.math.pow(pred, 2) * neg_weights * neg_inds

    num_pos = tf.reduce_sum(pos_inds)
    pos_loss = tf.reduce_sum(pos_loss)
    neg_loss = tf.reduce_sum(neg_loss)

    loss = tf.cond(tf.equal(num_pos, 0), lambda: -neg_loss, lambda: -(pos_loss + neg_loss) / num_pos)
    return loss


def CtdetLoss(output, batch):
    heatmap_loss = FocalLoss(output['heatmap'], batch['heatmap'])
    regress_loss = tf.losses.absolute_difference(batch['wh'], output['wh'], batch['mask'],
                                                         reduction=tf.losses.Reduction.MEAN)
    offset_loss = tf.losses.absolute_difference(batch['offset'], output['offset'], batch['mask'],
                                                        reduction=tf.losses.Reduction.MEAN)
    total_loss = heatmap_loss + 0.1 * regress_loss + offset_loss
    return total_loss, heatmap_loss, regress_loss, offset_loss
