import tensorflow as tf
from tensorflow.python.training import saver as tf_saver
from tensorflow.python.framework import graph_util
from model import LoadModel 
from config import parse_args

args = parse_args()
args.network = 'resnet50_v2_sparse2'
#args.ckptPath = '../../ckpts/exp01_city_resnet50_v2_img512_1p4bbox'
#args.ckptPath = '/home/jiachao/JCLiu/detection/detection_mf_2020/city/utils/tf_serving/tf_serving_ckpts/exp01_city_resnet50_v2_img224_1p4bbox'
args.ckpt_fp = '../tf_serving/tf_serving_ckpts/exp09_city-30782img-shangyao_resnet50_v2_img512_1p4bbox_94sparsity/'
#args.save_fp= 'pb_files/exp09_city-30782img-shangyao_resnet50_v2_img512_1p4bbox_94sparsity.pb'
#args.save_fp= 'pb_files/exp09_city-30782img-shangyao_resnet50_v2_1p4bbox_94sparsity_rgb_batch.pb'
args.save_fp = 'pb_files/modified_network_042420.pb'
args.input = 'rgb'

g = tf.Graph()
with g.as_default():
    model = LoadModel(args=args)
    input_graph_def = g.as_graph_def()

    '''
    latest_checkpoint = tf.train.latest_checkpoint(args.ckpt_fp)
    v_dict = dict()
    for v in tf.trainable_variables():
        if '53' not in v.name:
            v_dict[v.name[:-2]] = v
    saver = tf_saver.Saver(v_dict)
    '''

    sess = tf.Session()
    sess.run(tf.global_variables_initializer())
    #saver.restore(sess, latest_checkpoint)

    output_node_names = "detection"
    output_graph_def = graph_util.convert_variables_to_constants(
                        sess, # The session is used to retrieve the weights
                        input_graph_def, # The graph_def is used to retrieve the nodes 
                        output_node_names.split(",") # The output node names are used to select the usefull nodes
                                            )

    # Finally we serialize and dump the output graph to the filesystem
    with tf.gfile.GFile(args.save_fp, "wb") as f:
        f.write(output_graph_def.SerializeToString())
        print('*** {} ***'.format(args.save_fp))
