import tensorflow as tf
from config import parse_args

args = parse_args()
classes = args.classesList
num_classes = len(classes)

regularizer = tf.contrib.layers.l1_l2_regularizer(1e-7, 1e-7)


def deconv(x, kernel, in_channel, out_channel, stride, training, num):
    with tf.variable_scope('conv2d_transpose%s' % num):
        weight = tf.get_variable(
            name='weight',
            shape=[kernel, kernel, out_channel, in_channel],
            initializer=tf.initializers.glorot_uniform(),
            regularizer=regularizer)
        x = tf.nn.conv2d_transpose(
            value=x,
            filter=weight,
            output_shape=[tf.shape(x)[0], tf.shape(x)[1] * stride, tf.shape(x)[2] * stride,
                          out_channel],
            strides=stride,
            padding='SAME')
        x = tf.layers.batch_normalization(
            inputs=x,
            training=training)
        x = tf.nn.relu(x)
        return x


def detect(x, training):
    with tf.variable_scope('detect', reuse=tf.AUTO_REUSE):
        x = deconv(x, 4, x.shape.as_list()[-1], 256, 2, training, 1)
        x = deconv(x, 4, 256, 128, 2, training, 2)
        x = deconv(x, 4, 128, 64, 2, training, 3)

        with tf.variable_scope('heatmap'):
            weight1 = tf.get_variable(
                name='weight1',
                shape=[3, 3, 64, 64],
                initializer=tf.initializers.glorot_uniform(),
                regularizer=regularizer)
            bias1 = tf.get_variable('bias1', 64, initializer=tf.initializers.zeros())
            heatmap = tf.nn.conv2d(
                input=x,
                filter=weight1,
                strides=1,
                padding='SAME') + bias1
            heatmap = tf.nn.relu(heatmap)
            weight2 = tf.get_variable(
                name='weight2',
                shape=[1, 1, 64, num_classes],  # 80],
                initializer=tf.initializers.glorot_uniform(),
                regularizer=regularizer)
            bias2 = tf.get_variable('bias2', num_classes, initializer=tf.initializers.constant(-2.19))
            heatmap = tf.nn.conv2d(
                input=heatmap,
                filter=weight2,
                strides=1,
                padding='VALID') + bias2

        with tf.variable_scope('wh'):
            weight1 = tf.get_variable(
                name='weight1',
                shape=[3, 3, 64, 64],
                initializer=tf.initializers.glorot_uniform(),
                regularizer=regularizer)
            bias1 = tf.get_variable('bias1', 64, initializer=tf.initializers.zeros())
            wh = tf.nn.conv2d(
                input=x,
                filter=weight1,
                strides=1,
                padding='SAME') + bias1
            wh = tf.nn.relu(wh)
            weight2 = tf.get_variable(
                name='weight2',
                shape=[1, 1, 64, 2],
                initializer=tf.initializers.glorot_uniform(),
                regularizer=regularizer)
            bias2 = tf.get_variable('bias2', 2, initializer=tf.initializers.zeros())
            wh = tf.nn.conv2d(
                input=wh,
                filter=weight2,
                strides=1,
                padding='VALID') + bias2

        with tf.variable_scope('offset'):
            weight1 = tf.get_variable(
                name='weight1',
                shape=[3, 3, 64, 64],
                initializer=tf.initializers.glorot_uniform(),
                regularizer=regularizer)
            bias1 = tf.get_variable('bias1', 64, initializer=tf.initializers.zeros())
            offset = tf.nn.conv2d(
                input=x,
                filter=weight1,
                strides=1,
                padding='SAME') + bias1
            offset = tf.nn.relu(offset)
            weight2 = tf.get_variable(
                name='weight2',
                shape=[1, 1, 64, 2],
                initializer=tf.initializers.glorot_uniform(),
                regularizer=regularizer)
            bias2 = tf.get_variable('bias2', 2, initializer=tf.initializers.zeros())
            offset = tf.nn.conv2d(
                input=offset,
                filter=weight2,
                strides=1,
                padding='VALID') + bias2
    return {'heatmap': heatmap, 'wh': wh, 'offset': offset}
