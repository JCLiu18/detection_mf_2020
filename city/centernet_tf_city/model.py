import numpy
import tensorflow as tf
import cv2
import utils
import network.loss as loss
import os
from config import parse_args
from pycocotools import coco
import network.detector as detector 
import re

args = parse_args()
if args.network == 'resnet50_v2':
    import network.resnet_v2 as resnet_v2 
elif args.network == 'resnet50_v2_sparse':
    import network.resnet_v2_sparse as resnet_v2_sparse 

train_info = coco.COCO(args.train_annotate_path)
train_id = train_info.getImgIds()
val_info = coco.COCO(args.val_annotate_path)
val_id = val_info.getImgIds()
cat_id = train_info.getCatIds()
batch_size = args.batch_size
repeat = args.repeat
mean = numpy.array([0.40789654, 0.44719302, 0.47026115], dtype=numpy.float32).reshape(1, 1, 3)
std = numpy.array([0.28863828, 0.27408164, 0.27809835], dtype=numpy.float32).reshape(1, 1, 3)
data_rng = numpy.random.RandomState(123)
eig_val = numpy.array([0.2141788, 0.01817699, 0.00341571], dtype=numpy.float32)
eig_vec = numpy.array([
    [-0.58752847, -0.69563484, 0.41340352],
    [-0.5832747, 0.00994535, -0.81221408],
    [-0.56089297, 0.71832671, 0.41158938]], dtype=numpy.float32)

classes = args.classesList
num_classes = len(classes)
class_to_ind = dict(zip(classes, range(num_classes)))

total_data = len(train_id)
total_data_val = len(val_id)
print('*** train image: {}    val image: {} ***'.format(total_data, total_data_val))
max_step = numpy.ceil(total_data * repeat / batch_size).astype(numpy.int32)
summary_step = args.summary_step  
checkpoint_step = args.checkpoint_step  
step = numpy.ceil(max_step / repeat).astype(int).tolist()
init_lr = args.base_lr / 128 * (batch_size)
decay_lr = 1e-1
decay_epoch = [10, 20]
decay_step = [one * step for one in decay_epoch]
step_lr = []
for i in range(len(decay_step) + 1):
    step_lr.append(init_lr * (decay_lr ** i))


def get_network(x, training):
    if args.network == 'resnet50_v2':
        x, _ = resnet_v2.ImagenetModel(resnet_size=50, data_format='channels_last', num_classes=num_classes)(x,training=training)
    elif args.network == 'resnet50_v2_sparse':
        x = resnet_v2_sparse.ResNet_50(x, training)

    logit = detector.detect(x, training)
    return logit


def model_fn(features, labels, mode):
    features.set_shape([None, args.img_size, args.img_size, 3])
    output_size = args.img_size // 4
    labels.set_shape([None, output_size, output_size, num_classes + 6])

    heatmap = labels[:, :, :, :num_classes]
    wh = labels[:, :, :, num_classes:num_classes + 2]
    offset = labels[:, :, :, num_classes + 2:num_classes + 2 + 2]
    mask = labels[:, :, :, num_classes + 2 + 2:]

    if mode == tf.estimator.ModeKeys.TRAIN:
        predict = get_network(features, True)

        global_step = tf.train.get_or_create_global_step()
        learning_rate = tf.train.piecewise_constant_decay(global_step, decay_step, step_lr)
        Optimizer = tf.train.AdamOptimizer(learning_rate)

        train_loss, heatmap_loss, regress_loss, offset_loss = loss.CtdetLoss(predict, {'heatmap': heatmap, 'wh': wh,
                                                                                       'offset': offset, 'mask': mask})

        tf.summary.scalar('train_loss', train_loss)
        tf.summary.scalar('train_heatmap_loss', heatmap_loss)
        tf.summary.scalar('train_regress_loss', regress_loss)
        tf.summary.scalar('train_offset_loss', offset_loss)
        tf.summary.scalar('learning_rate', learning_rate)

        def count_flops(A, W, inp_chan_mode_A=3, inp_chan_mode_W=2):
            Annz_c = tf.count_nonzero(A, [d for d in range(len(A.shape)) if d != inp_chan_mode_A])
            Annz_c = tf.cast(Annz_c, tf.float32)
            Wnnz_c = tf.count_nonzero(W, [d for d in range(len(W.shape)) if d != inp_chan_mode_W])
            Wnnz_c = tf.cast(Wnnz_c, tf.float32)
            f = tf.reduce_sum(Wnnz_c * Annz_c) / tf.cast(tf.shape(A)[0], tf.float32)
            return f

        def count_dense_flops(A, W, inp_chan_mode_A=3, inp_chan_mode_W=2):
            A_shape = tf.shape(A)
            W_shape = tf.shape(W)
            B = tf.cast(A_shape[0], tf.float32)
            C = tf.cast(W_shape[inp_chan_mode_W], tf.float32)
            BCHW = tf.cast(tf.reduce_prod(A_shape), tf.float32)
            RSCK = tf.cast(tf.reduce_prod(W_shape), tf.float32)
            return BCHW * RSCK / B / C

        #Counting Weight NNZ
        W_dict = [one for one in tf.global_variables() if re.match('.*conv2d_.*/kernel', one.name) != None]
        w_nnz_dict = dict([one.name, tf.count_nonzero(one)] for one in W_dict)
        w_size_dict = dict([one.name, tf.cast(tf.size(one), tf.float32)] for one in W_dict)
        w_nnz = tf.add_n(list(w_nnz_dict.values()))
        w_size = tf.add_n(list(w_size_dict.values()))
        w_nnz_ratio = tf.cast(w_nnz, tf.float32) / w_size
        #Counting Act NNZ
        A_W_dict = dict([op.name, [op.inputs[i] for i in [-2, -1]]] for op in tf.get_default_graph().get_operations() if re.match('.*conv2d_.*/Conv2D$', op.name) != None)
        act_nnz_dict = dict((k, tf.cast(tf.count_nonzero(A), tf.float32) / tf.cast(tf.shape(A)[0], tf.float32)) for k, (A, W) in A_W_dict.items())
        act_size_dict = dict((k, tf.cast(tf.size(A), tf.float32) / tf.cast(tf.shape(A)[0], tf.float32)) for k, (A, W) in A_W_dict.items())
        act_nnz = tf.add_n(list(act_nnz_dict.values()))
        act_size = tf.add_n(list(act_size_dict.values()))
        act_nnz_ratio = act_nnz / act_size
        #Counting FLOPs
        flops_dict = dict((k, count_flops(A, W)) for k, (A, W) in A_W_dict.items())
        flops = tf.add_n(list(flops_dict.values()))
        dense_flops_dict = dict((k, count_dense_flops(A, W)) for k, (A, W) in A_W_dict.items())
        dense_flops = tf.add_n(list(dense_flops_dict.values()))
        flops_ratio = flops / dense_flops

        mean_w_nnz_ratio = tf.metrics.mean(w_nnz_ratio)
        mean_act_nnz_ratio = tf.metrics.mean(act_nnz_ratio)
        mean_flops_ratio = tf.metrics.mean(flops_ratio)
        metrics = {
            'mean_w_nnz_ratio': mean_w_nnz_ratio,
            'mean_act_nnz_ratio': mean_act_nnz_ratio,
            'mean_flops_ratio': mean_flops_ratio}

        tf.summary.scalar('nnz/mean_w_nnz_ratio', w_nnz_ratio)
        tf.summary.scalar('nnz/mean_act_nnz_ratio', act_nnz_ratio)
        tf.summary.scalar('nnz/mean_flops_ratio', flops_ratio)

        with tf.control_dependencies(tf.get_collection(tf.GraphKeys.UPDATE_OPS)):
            train_op = Optimizer.minimize(train_loss, global_step, [one for one in tf.trainable_variables() if not (('beta' in one.name or 'gamma' in one.name) and ('resnet' in one.name))])

        EstimatorSpec = tf.estimator.EstimatorSpec(
            mode=tf.estimator.ModeKeys.TRAIN,
            loss=train_loss,
            train_op=train_op)
        return EstimatorSpec

    if mode == tf.estimator.ModeKeys.EVAL:
        predict = get_network(features, False)

        validate_loss, validate_heatmap_loss, validate_regress_loss, validate_offset_loss = loss.CtdetLoss(predict, {'heatmap': heatmap, 'wh': wh,
                                                                                          'offset': offset,
                                                                                          'mask': mask})


        EstimatorSpec = tf.estimator.EstimatorSpec(
            mode=tf.estimator.ModeKeys.EVAL,
            loss=validate_loss,
        )
        return EstimatorSpec

    if mode == tf.estimator.ModeKeys.PREDICT:
        pass


def train_fn():
    def parse_train(temp_id):
        temp_id = temp_id.tolist()
        image_info = train_info.loadImgs(ids=temp_id)
        ann_id = train_info.getAnnIds(imgIds=temp_id)
        ann_info = train_info.loadAnns(ids=ann_id)
        #image_path = os.path.join(args.train_image_path, image_info[0]['file_name'])
        image_path = image_info[0]['file_name']
        image = cv2.imread(image_path)
        height, width = image.shape[0], image.shape[1]  

        c = numpy.array([width / 2., height / 2.])
        s = max(height, width)
        input_h, input_w = args.img_size, args.img_size
        output_h, output_w = input_h // 4, input_w // 4

        border_tmp_h, border_tmp_w = 128, 128

        s = s * numpy.random.choice(numpy.arange(0.6, 1.4, 0.1))
        #s = s * numpy.random.choice(numpy.arange(0.9, 1.1, 0.1))
        i = 1
        while width - border_tmp_w // i <= border_tmp_w // i:
            i = i * 2
        w_border = border_tmp_w // i
        i = 1
        while height - border_tmp_h // i <= border_tmp_h // i:
            i = i * 2
        h_border = border_tmp_h // i
        c[0] = numpy.random.randint(low=w_border, high=width - w_border)
        c[1] = numpy.random.randint(low=h_border, high=height - h_border)

        if numpy.random.random() < 0.5:
            flipped = True
            image = image[:, ::-1, :]
            c[0] = width - c[0] - 1
        else:
            flipped = False

        trans_input = utils.get_affine_transform(c, s, 0, [input_w, input_h])
        crop_image = cv2.warpAffine(image, trans_input, (input_w, input_h), flags=cv2.INTER_LINEAR)
        crop_image = (crop_image.astype(numpy.float32) / 255.)
        utils.color_aug(data_rng, crop_image, eig_val, eig_vec)
        crop_image = (crop_image - mean) / std
        crop_image = cv2.cvtColor(crop_image, cv2.COLOR_BGR2RGB)
        crop_image = crop_image.transpose(2, 0, 1)
        trans_output = utils.get_affine_transform(c, s, 0, [output_w, output_h])

        max_objs = 128
        heatmap = numpy.zeros((num_classes, output_h, output_w), dtype=numpy.float32)
        wh = numpy.zeros((2, output_h, output_w), dtype=numpy.float32)
        offset = numpy.zeros((2, output_h, output_w), dtype=numpy.float32)
        mask = numpy.zeros((2, output_h, output_w), dtype=numpy.float32)


        for k in range(min(len(ann_info), max_objs)):
            ann = ann_info[k]
            box = ann['bbox']
            new_box = [max(0, box[0]-box[2]*0.2), max(0, box[1] - box[3]*0.2), min(box[0] + box[2]*1.2, width), min(box[1]+box[3]*1.2, height)]
            bbox = numpy.array(new_box, dtype=numpy.float32)
            #bbox = numpy.array([box[0], box[1], box[0] + box[2], box[1] + box[3]], dtype=numpy.float32)

            cls_id = cat_id.index(ann['category_id'])
            if flipped:
                bbox[[0, 2]] = width - bbox[[2, 0]] - 1

            bbox[:2] = utils.affine_transform(bbox[:2], trans_output)
            bbox[2:] = utils.affine_transform(bbox[2:], trans_output)
            bbox[[0, 2]] = numpy.clip(bbox[[0, 2]], 0, output_w - 1)
            bbox[[1, 3]] = numpy.clip(bbox[[1, 3]], 0, output_h - 1)
            h, w = bbox[3] - bbox[1], bbox[2] - bbox[0]
            if h > 0 and w > 0:
                radius = utils.gaussian_radius((numpy.ceil(h), numpy.ceil(w)))
                radius = max(0, int(radius))
                ct = numpy.array([(bbox[0] + bbox[2]) / 2, (bbox[1] + bbox[3]) / 2], dtype=numpy.float32)
                ct_int = ct.astype(numpy.int32)
                utils.draw_umich_gaussian(heatmap[cls_id], ct_int, radius)
                wh[:, ct_int[1], ct_int[0]] = [w, h]
                offset[:, ct_int[1], ct_int[0]] = ct - ct_int
                mask[:, ct_int[1], ct_int[0]] = [1, 1]
        label = numpy.concatenate([heatmap, wh, offset, mask])
        crop_image = numpy.transpose(crop_image, [1, 2, 0])
        label = numpy.transpose(label, [1, 2, 0])
        return crop_image, label

    dataset = tf.data.Dataset.from_tensor_slices(train_id).shuffle(batch_size * 10).repeat(repeat).map(
        lambda temp_id: tf.numpy_function(parse_train, [temp_id], [tf.float32, tf.float32]),
        -1).batch(batch_size).prefetch(-1)
    return dataset


def eval_fn():
    def parse_validate(temp_id):
        temp_id = temp_id.tolist()
        image_info = val_info.loadImgs(ids=temp_id)
        ann_id = val_info.getAnnIds(imgIds=temp_id)
        ann_info = val_info.loadAnns(ids=ann_id)
        #image_path = os.path.join(args.train_image_path, image_info[0]['file_name'])
        image_path = image_info[0]['file_name']
        image = cv2.imread(image_path)
        height, width = image.shape[0], image.shape[1]

        c = numpy.array([width / 2., height / 2.])
        s = max(height, width)
        input_h, input_w = args.img_size, args.img_size
        output_h, output_w = input_h // 4, input_w // 4

        border_tmp_h, border_tmp_w = 128, 128  

        s = s * numpy.random.choice(numpy.arange(0.6, 1.4, 0.1))
        i = 1

        while width - border_tmp_w // i <= border_tmp_w // i:
            i = i * 2
        w_border = border_tmp_w // i
        i = 1
        while height - border_tmp_h // i <= border_tmp_h // i:
            i = i * 2
        h_border = border_tmp_h // i
        c[0] = numpy.random.randint(low=w_border, high=width - w_border)
        c[1] = numpy.random.randint(low=h_border, high=height - h_border)

        if numpy.random.random() < 0.5:
            flipped = True
            image = image[:, ::-1, :]
            c[0] = width - c[0] - 1
        else:
            flipped = False

        trans_input = utils.get_affine_transform(c, s, 0, [input_w, input_h])
        crop_image = cv2.warpAffine(image, trans_input, (input_w, input_h), flags=cv2.INTER_LINEAR)
        crop_image = (crop_image.astype(numpy.float32) / 255.)
        utils.color_aug(data_rng, crop_image, eig_val, eig_vec)
        crop_image = (crop_image - mean) / std
        crop_image = cv2.cvtColor(crop_image, cv2.COLOR_BGR2RGB)
        crop_image = crop_image.transpose(2, 0, 1)
        trans_output = utils.get_affine_transform(c, s, 0, [output_w, output_h])

        max_objs = 128
        heatmap = numpy.zeros((num_classes, output_h, output_w), dtype=numpy.float32)
        wh = numpy.zeros((2, output_h, output_w), dtype=numpy.float32)
        offset = numpy.zeros((2, output_h, output_w), dtype=numpy.float32)
        mask = numpy.zeros((2, output_h, output_w), dtype=numpy.float32)

        for k in range(min(len(ann_info), max_objs)):
            ann = ann_info[k]
            box = ann['bbox']
            bbox = numpy.array([box[0], box[1], box[0] + box[2], box[1] + box[3]], dtype=numpy.float32)
            cls_id = cat_id.index(ann['category_id'])
            if flipped:
                bbox[[0, 2]] = width - bbox[[2, 0]] - 1
            bbox[:2] = utils.affine_transform(bbox[:2], trans_output)
            bbox[2:] = utils.affine_transform(bbox[2:], trans_output)
            bbox[[0, 2]] = numpy.clip(bbox[[0, 2]], 0, output_w - 1)
            bbox[[1, 3]] = numpy.clip(bbox[[1, 3]], 0, output_h - 1)
            h, w = bbox[3] - bbox[1], bbox[2] - bbox[0]
            if h > 0 and w > 0:
                radius = utils.gaussian_radius((numpy.ceil(h), numpy.ceil(w)))
                radius = max(0, int(radius))
                ct = numpy.array([(bbox[0] + bbox[2]) / 2, (bbox[1] + bbox[3]) / 2], dtype=numpy.float32)
                ct_int = ct.astype(numpy.int32)
                utils.draw_umich_gaussian(heatmap[cls_id], ct_int, radius)
                wh[:, ct_int[1], ct_int[0]] = [w, h]
                offset[:, ct_int[1], ct_int[0]] = ct - ct_int
                mask[:, ct_int[1], ct_int[0]] = [1, 1]
        label = numpy.concatenate([heatmap, wh, offset, mask])
        crop_image = numpy.transpose(crop_image, [1, 2, 0])
        label = numpy.transpose(label, [1, 2, 0])
        return crop_image, label

    dataset = tf.data.Dataset.from_tensor_slices(val_id).repeat(1).map(
        lambda temp_id: tf.numpy_function(parse_validate, [temp_id], [tf.float32, tf.float32]),
        -1).batch(batch_size).prefetch(-1)
    return dataset
