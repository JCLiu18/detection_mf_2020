import sys
import pandas
import os
import tensorflow as tf
import numpy as np

os.environ['CUDA_VISIBLE_DEVICES'] = '3'
#ckpt_path = './checkpoints/pretrainCity_resnet50_v2_32_512_256_112019'
#meta_path = 'checkpoint/pretrain_imgnet_resnet34_v2/model.ckpt-125114.meta'
#meta_path = '/ssd/JC/city_detect/checkpoints/exp05_city_resnet50_v2_AWsparse_img224_1p2bbox/model.ckpt-141000.meta'
#ckpt_path = '/ssd/JC/city_detect/checkpoints/exp05_city_resnet50_v2_AWsparse_img224_1p2bbox/model.ckpt-141000'
#meta_path = 'checkpoints/pretrainCity_resnet50_v2_011320_AWSparse_map13p9/model.ckpt-86251.meta'
#ckpt_path = 'checkpoints/pretrainCity_resnet50_v2_011320_AWSparse_map13p9/model.ckpt-86251'
#meta_path = 'checkpoints/exp07_city_resnet50_v2_AWsparse_img224_1p4bbox/model.ckpt-42000.meta'
#ckpt_path = 'checkpoints/exp07_city_resnet50_v2_AWsparse_img224_1p4bbox/model.ckpt-42000'
#meta_path = 'ckpt/exp01_city_resnet50_v2_img512_1p4bbox/model.ckpt-36000.meta'
#ckpt_path = 'ckpt/exp01_city_resnet50_v2_img512_1p4bbox/model.ckpt-36000'
meta_path = 'ckpt/exp02_city_resnet50_v2_sparse_img512_1p4bbox/model.ckpt-33000.meta'
ckpt_path = 'ckpt/exp02_city_resnet50_v2_sparse_img512_1p4bbox/model.ckpt-33000'




saver = tf.train.import_meta_graph(meta_path)

#for v in tf.global_variables():
#    print(v.name, ": ", v.shape)

w_dict = dict()
#graph = tf.get_default_graph() 
#w = graph.get_tensor_by_name('student/resnet_model/conv2d_52/kernel/MomentumPrune_1:0') 
for v in tf.global_variables():
    if 'MomentumPrune' in v.name:
        w_dict[v.name] = v

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    saver.restore(sess, ckpt_path) 

    #W = sess.run(w)
    W_dict = sess.run(w_dict)
    for k,v in W_dict.items():
        print(k, '_nnz: ', np.count_nonzero(v)/v.size)

    #print(np.count_nonzero(W)/W.size)
