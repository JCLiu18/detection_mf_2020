#python3 train.py \
#		--GPU=0 \
#		--train_annotate_path=lyy_data/city_26000imgs/train.json \
#		--val_annotate_path=lyy_data/city_26000imgs/test.json \
#		--img_size=512 \
#		--batch_size=12 \
#		--network=resnet50_v2 \
#		--ckptPath=ckpt/exp01_city_resnet50_v2_img512_1p4bbox \
#		--resnet_variables_to_restore=network/resnet_v2_variables_to_restore \
#		--ckpt_to_initialize_from=ckpt/pretrainCoco_resnet50_v2


python3 train.py \
		--GPU=1 \
		--train_annotate_path=lyy_data/city_26000imgs/train.json \
		--val_annotate_path=lyy_data/city_26000imgs/test.json \
		--img_size=512 \
		--batch_size=12 \
		--network=resnet50_v2_sparse \
		--ckptPath=ckpt/exp02_city_resnet50_v2_sparse_img512_1p4bbox \
		--resnet_variables_to_restore=network/resnet_v2_sparse_variables_to_restore \
		--ckpt_to_initialize_from=ckpt/pretrainCoco_resnet50_v2_sparse

