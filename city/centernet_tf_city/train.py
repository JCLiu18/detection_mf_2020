import pandas
import tensorflow
from model import train_fn, eval_fn, model_fn
import os
from config import parse_args
import sys
sys.path.append(os.path.join(os.path.dirname(__file__), "network"))


args = parse_args()
tensorflow.logging.set_verbosity(tensorflow.logging.INFO)
session_config = tensorflow.ConfigProto(
    allow_soft_placement=True,
    gpu_options=tensorflow.GPUOptions(allow_growth=True),
    inter_op_parallelism_threads=0,
    intra_op_parallelism_threads=0)

run_config = tensorflow.estimator.RunConfig(
    model_dir=args.ckptPath,
    save_summary_steps=args.summary_step,
    save_checkpoints_steps=args.checkpoint_step,
    session_config=session_config,
    keep_checkpoint_max=2,
    log_step_count_steps=100,
    eval_distribute=None)

def train(args):
    variables_to_restore = pandas.read_pickle(args.resnet_variables_to_restore)
    WarmStartSettings = tensorflow.estimator.WarmStartSettings(
        ckpt_to_initialize_from=args.ckpt_to_initialize_from,
        vars_to_warm_start=variables_to_restore)
        #vars_to_warm_start='.*')
    Estimator = tensorflow.estimator.Estimator(model_fn=model_fn, config=run_config, warm_start_from=WarmStartSettings)

    TrainSpec = tensorflow.estimator.TrainSpec(input_fn=train_fn, max_steps=args.max_step)
    EvalSpec = tensorflow.estimator.EvalSpec(input_fn=eval_fn, steps=None, start_delay_secs=0, throttle_secs=600)
    tensorflow.estimator.train_and_evaluate(Estimator, TrainSpec, EvalSpec)


if __name__ == '__main__':
    os.environ["CUDA_VISIBLE_DEVICES"] = args.GPU
    train(args)
