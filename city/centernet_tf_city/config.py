import numpy as np
from pycocotools import coco
import argparse


def parse_args():
    parser = argparse.ArgumentParser(description='train')
    parser.add_argument('--GPU', help='GPU', default='1', type=str)
    parser.add_argument('--img_size', help='img_size', default=512, type=int)
    parser.add_argument('--batch_size', help='batch_size', default=12, type=int)
    parser.add_argument('--repeat', help='repeat', default=200, type=int)
    parser.add_argument('--max_step', help='max_step', default=20000000000, type=int)
    parser.add_argument('--base_lr', help='init_lr', default=5e-4, type=float)
    parser.add_argument('--classesList', help='classes  list', default=['peddler', 'occupy', 'exposure', 'billboard', 'hanger', 'noparking', 'cover', 'trashbag', 'stack', 'trashcan'], type=list)

    parser.add_argument('--train_annotate_path', help='train json path', default='../data/train_26k10cls_121019.json', type=str)

    parser.add_argument('--val_annotate_path', help='val json path', default='../data/val_26k10cls_121019.json', type=str)
    
    parser.add_argument('--network', help='resnet50_v2, resnet50_v2_sparse',
                        default='resnet50_v2',
                        type=str)

    parser.add_argument('--ckptPath', help='save checkpoints path',
                        default='ckpt/test_ckpt/')

    parser.add_argument('--ckpt_to_initialize_from', help='ckpt to initialize from',
                        default='ckpt/pretrainCoco_resnet50_v2/',
                        type=str)

    parser.add_argument('--resnet_variables_to_restore', help='resnet variables to restore',
                        default='network/resnet_v2_variables_to_restore',
                        type=str)

    parser.add_argument('--summary_step', help='summary_step', default=100, type=int)
    parser.add_argument('--checkpoint_step', help='checkpoint step save', default=3000, type=int)
    parser.add_argument('--decay_epoch', help='decay_epoch', default=[10, 20], type=list)
    args = parser.parse_args()

    return args

