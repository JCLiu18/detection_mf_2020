import json
import os
from pycocotools import coco

'''
def coco_stats(coco_jsn, cats, cat_map):
    coco_info = coco.COCO(coco_jsn)
    img_ids = sorted(coco_info.getImgIds())
    cat_ids = coco_info.getCatIds()

    for img_id in img_ids:
        image_info = coco_info.loadImgs(ids=img_id)
        ann_ids = coco_info.getAnnIds(imgIds=img_id)
        ann_info = coco_info.loadAnns(ids=ann_ids)
        coco_dict[image_info[0]['file_name']] = ann_info
'''

def coco_stats(coco_jsn, target_cats):
    coco_info = coco.COCO(coco_jsn)
    #cat_info = coco_info.loadCats(coco_info.getCatIds())
    #cat_nms = [cat['name'] for cat in cat_info]

    img_set = set() 
    for cat_id in target_cats:
        cat_info = coco_info.loadCats(cat_id)
        img_ids = coco_info.getImgIds(catIds=cat_id)
        ann_ids = coco_info.getAnnIds(imgIds=img_ids)
        print(ann_ids)
        print('{}, {}'.format(cat_info[0]['name'], len(set(ann_ids))))
        exit()

        img_set.update(img_ids)

    print('totol imgs: ', len(img_set))



if __name__ == '__main__':
    inp_jsn = '../../../../data/COCO/annotations/instances_val2017.json'

    # person,   nonvehicle: (bicycle, motorcycle),   vehicle: (car, bus, truck)
    target_cats = [2, 3, 4, 6, 8]  # person, bicycle, car, motocycle, bus, truck
    cat_map = dict(person=[1],nonvehicle=[2,4],vehicle=[3,6,8])
    coco_stats(inp_jsn, target_cats)

