import json

jsn_fp = 'data/Body_C1_20191028.json'
out_fp = 'data/Body_C1_20191028_subset1/Body_C1_20191028_subset1.json'
min_order = 10010
max_order = 10019

with open(jsn_fp, 'r') as inp:
    dataset = json.load(inp)
img_info = dataset['frames']  

out_info = {}
for img_name, bboxs in img_info.items():
    order = int(img_name.split('.')[0][-5:])

    if order < min_order:
        continue
    if order > max_order:
        break

    print(img_name)
    out_info[img_name] = bboxs

out_dict = {'frames':out_info}

with open(out_fp, 'w') as out:
    json.dump(out_dict, out, indent=2)


