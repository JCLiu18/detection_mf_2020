###03/02/2020
#### vott2coco, draw_coco, draw_vott
1. run `draw_vott.py`, drawn images saved in draw_vott 
2. run `vott2coco.py` to generate annotation json in `annotation`
3. run `split_annotation.py` to split the annotation json into test and train
4. run `draw_coco.py` to draw figures from coco annotation file
