import os
import json
import cv2
import colorsys


def draw_vott(jsn_fp, classes, out_fp):
    num_classes = len(classes)
    class_to_ind = dict(zip(classes, range(1, num_classes + 1)))
    hsv_tuples = [(1.0 * x / num_classes, 1., 1.) for x in range(num_classes)]
    colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
    colors = list(map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), colors))

    cnt_img = 0
    cnt_bbox = 0
    cnt_noimg = 0
    cnt_dict = dict()
    for cls in classes:
        cnt_dict[cls] = 0

    with open(jsn_fp, 'r') as inp:
        dataset = json.load(inp)
    img_info = dataset['frames']  
    root, jsn_name = jsn_fp.split('/')
    img_root= os.path.join(root, jsn_name.split('.')[0])
    img_fps = {}
    for root, dirs, files in os.walk(img_root):
        for f in files:
            if f.endswith('.jpg'):
                img_fp = os.path.join(root, f)
                img_fps[f] = img_fp 

    print('*** draw images from {} ***'.format(img_root)) 
    for img_name, bboxs in img_info.items():
        if img_name not in img_fps.keys():
            print('{} not in the img folder'.format(img_name))
            cnt_noimg += 1
        else:
            img = cv2.imread(img_fps[img_name])
            cnt_img += 1

            for bbox in bboxs:
                cnt_bbox += 1
                xmin, ymin, xmax, ymax = int(bbox['box']['x1']), int(bbox['box']['y1']), \
                        int(bbox['box']['x2']), int(bbox['box']['y2'])
                label = bbox['tags'][0]
                cnt_dict[label] += 1
                cv2.rectangle(img, (xmin, ymin), (xmax, ymax), colors[class_to_ind[label] - 1], 3)
                #cv2.putText(img, label, (xmin, ymin - 10), cv2.FONT_HERSHEY_COMPLEX, 2, (166, 166, 166), 3)
            cv2.imwrite(os.path.join(out_fp, img_name), img)

    print('*** processed {} imgs ***'.format(cnt_img))
    print('*** processed {} bboxs ***'.format(cnt_bbox))
    print('*** {} imgs are not found ***'.format(cnt_noimg))

    for k, v in cnt_dict.items():
        print('{}: {} bboxs'.format(k, v)) 


if __name__ == '__main__':
    jsn_fp = 'data/Body_C1_20191028_subset0.json'
    out_fp = 'draw_vott/Body_C1_20191028_subset0'  
    classes = ['person', 'nonvehicle', 'vehicle', 'face']

    if not os.path.exists(out_fp):
        os.makedirs(out_fp)

    draw_vott(jsn_fp, classes, out_fp)
