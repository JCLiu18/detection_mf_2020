# coding: utf-8
import json
from collections import defaultdict
import os
import cv2
from pycocotools import coco
import colorsys

def decode_coco(coco_jsn):
    coco_info = coco.COCO(coco_jsn)
    img_ids = coco_info.getImgIds()
    cat_ids = coco_info.getCatIds()
    draw_info = dict()
    for img_id in img_ids:
        image_info = coco_info.loadImgs(ids=img_id)
        ann_ids = coco_info.getAnnIds(imgIds=img_id)
        ann_info = coco_info.loadAnns(ids=ann_ids)
        draw_info[image_info[0]['file_name']] = ann_info
    return draw_info


def draw(draw_info, classes, out_root):
    img_cnt = 0
    bbox_cnt = 0
    num_classes = len(classes)
    class_to_ind = dict(zip(classes, range(1, num_classes + 1)))
    hsv_tuples = [(1.0 * x / num_classes, 1., 1.) for x in range(num_classes)]
    colors = list(map(lambda x: colorsys.hsv_to_rgb(*x), hsv_tuples))
    colors = list(map(lambda x: (int(x[0] * 255), int(x[1] * 255), int(x[2] * 255)), colors))
    cnt_dict = dict()
    for cls in classes:
        cnt_dict[cls] = 0

    for img_fp, info in draw_info.items():
        img_cnt += 1
        img_name = img_fp.split('/')[-1]
        img = cv2.imread(img_fp)

        out_fp = os.path.join(out_root, img_fp.replace(img_name, '')) 

        if not os.path.exists(out_fp):
            os.makedirs(out_fp)
        
        for bbox_info in info:
            bbox_cnt += 1
            label = cls_list[bbox_info['category_id']-1]
            cnt_dict[label] += 1
            bbox = bbox_info['bbox']
            xmin, ymin, xmax, ymax = int(bbox[0]), int(bbox[1]), int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3])

            cv2.rectangle(img, (xmin, ymin), (xmax, ymax), colors[class_to_ind[label] - 1], 3)
            #cv2.putText(img, label, (xmin, ymin - 10), cv2.FONT_HERSHEY_COMPLEX, 2, (166, 166, 166), 3)
        cv2.imwrite(os.path.join(out_fp, img_name), img)
    print('*** annotated images saved in {} ***'.format(out_fp))
    print('*** {} imgs and {} bboxs ***'.format(img_cnt, bbox_cnt))

    for k, v in cnt_dict.items():
        print('{}: {} bboxs'.format(k, v)) 


if __name__ == '__main__':
    jsn_fp = 'annotation/vott2coco_Body_C1_20191028_subsets.json'
    out_root = 'draw_coco/vott2coco_Body_C1_20191028_subsets'
    cls_list = ['person', 'nonvehicle', 'vehicle', 'face']

    if not os.path.exists(out_root):
        os.makedirs(out_root)

    draw_info = decode_coco(jsn_fp)
    draw(draw_info, cls_list, out_root)
