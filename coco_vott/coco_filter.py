import json
import os
import argparse
from random import shuffle
import subprocess as sub

arg_parser = argparse.ArgumentParser()
arg_parser.add_argument('--inp_img', default='../../Dataset/coco/train2017/')
arg_parser.add_argument('--inp_jsn', default='../../Dataset/coco/annotations/instances_train2017.json')
arg_parser.add_argument('--out_img', default='../../Dataset/coco/coco_subset/train2017/')
arg_parser.add_argument('--out_jsn', default='../../Dataset/coco/coco_subset/annotations/instances_train2017.json')
args = arg_parser.parse_args()

if not os.path.isdir(args.out_img):
    os.makedirs(args.out_img)

if not os.path.isdir(args.out_jsn.replace(args.out_jsn.split('/')[-1], '')):
    os.makedirs(args.out_jsn.replace(args.out_jsn.split('/')[-1], ''))

def parse(cls_list):
    out = dict()
    cnt = 0 
    with open(args.inp_jsn, 'r') as fpr:
        cocoInfo = json.load(fpr)
        # cocoInfo.keys(): dict_keys(['licenses', 'info', 'annotations', 'categories', 'images'])
        # 'images': ['coco_url', 'height', 'file_name', 'width', 'license', 'date_captured', 'id', 'flickr_url']
        # 'annotations': ['segmentation', 'bbox', 'id', 'category_id', 'area', 'image_id', 'iscrowd']
        for key in ['licenses', 'info']:
            out[key] = cocoInfo[key]
        out['categories'] = []
        for i, cls in enumerate(cls_list):
            out['categories'].append({'supercategory': '{}'.format(cls), 'id': i + 1, 'name': '{}'.format(cls)})
        # person--1,   nonvehicle: (bicycle--2, motorcycle--4),   vehicle: (car--3, bus--6, truck--8)
        cats = [1, 2, 3, 4, 6, 8]  # person, bicycle, car, motocycle, bus, truck
        cat_list = list(map(str, cats))
        out['annotations'] = list()

        img_set = set()
        #shuffle(cocoInfo['annotations'])
        vehicle_count = 0
        nonvehicle_count = 0
        person_count = 0
        for index, anno in enumerate(cocoInfo['annotations']):
            cat = str(anno['category_id'])
            if cat in cat_list:
                if cat in ['3', '6', '8']:
                    anno['category_id'] = 3
                    vehicle_count += 1
                elif cat in ['2', '4']:
                    anno['category_id'] = 2
                    nonvehicle_count += 1
                else:
                    anno['category_id'] = 1
                    person_count += 1
                out['annotations'].append(anno)
                img_set.add(anno['image_id'])

        out['images'] = list()
        for img in cocoInfo['images']:
            if img['id'] in img_set:
                out['images'].append(img)

        for img in out['images']:
            pimg = os.path.join(args.inp_img, img['file_name'])
            cimg = os.path.join(args.out_img, img['file_name'])
            sub.call(['cp', pimg, cimg])
            cnt += 1
            if cnt % 500 == 0:
                print('processed {} imgs'.format(cnt))
        print('*** {} imgs in total, person: {}  nonvehicle: {}  vehicle: {} ***'.format(cnt, person_count, nonvehicle_count, vehicle_count))

    with open(args.out_jsn, 'w') as fpr:
        json.dump(out, fpr)
        print('** coco subset preparation is done! **')


if __name__ == '__main__':
    # person,   nonvehicle: (bicycle, motorcycle),   vehicle: (car, bus, truck)
    args.inp_jsn = '../../../../data/COCO/annotations/instances_val2017.json'
    args.inp_img = '../../../../data/COCO/val2017'
    args.out_jsn = '../../../../data/COCO/annotations/instances_val2017_subset.json'
    args.out_img = '../../../../data/COCO/val2017_subset'

    cls_list = ['person', 'nonvehicle', 'vehicle']
    parse(cls_list)
