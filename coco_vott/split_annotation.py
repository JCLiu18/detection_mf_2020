from pycocotools.coco import COCO
import json
import random
import os

val_size = 10
anno_file = "annotation/vott2coco_Body_C1_20191028_subsets.json"
save_dir = anno_file.replace(anno_file.split('/')[-1], '') 
if not os.path.exists(save_dir):
    os.makedirs(save_dir)

coco = COCO(anno_file)
with open(anno_file, "r") as f:
    ir = json.load(f)

img_ids = coco.getImgIds()
val_ids = random.sample(img_ids, val_size)
train_ids = [i for i in img_ids if i not in val_ids]

train_imgs = []
val_imgs = []
train_annos = []
val_annos = []
categories = ir['categories']

for image in ir['images']:
    if image['id'] in train_ids:
        train_imgs.append(image)
    if image['id'] in val_ids:
        image['split'] ='test'
        val_imgs.append(image)


for anno in ir['annotations']:
    anno['area'] = anno['bbox'][2] * anno['bbox'][3]
    if anno['image_id'] in train_ids:
        train_annos.append(anno)
    if anno['image_id'] in val_ids:
        val_annos.append(anno)

train_jsn = os.path.join(save_dir, 'train.json')
test_jsn = os.path.join(save_dir, 'test.json')

with open(train_jsn, "w") as f:
    irf = dict(images=train_imgs, annotations=train_annos, categories=categories)
    json.dump(irf, f, indent=2)
with open(test_jsn, "w") as f:
    irf = dict(images=val_imgs, annotations=val_annos, categories=categories)
    json.dump(irf, f, indent=2)

print('split jsons saved as {}, {}'.format(train_jsn, test_jsn))
