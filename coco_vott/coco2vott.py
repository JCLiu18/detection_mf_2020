import json
from pycocotools import coco
import os
import cv2

def decode_coco(coco_jsn):
    coco_info = coco.COCO(coco_jsn)
    img_ids = coco_info.getImgIds()
    cat_ids = coco_info.getCatIds()
    draw_info = dict()
    for img_id in img_ids:
        image_info = coco_info.loadImgs(ids=img_id)
        ann_ids = coco_info.getAnnIds(imgIds=img_id)
        ann_info = coco_info.loadAnns(ids=ann_ids)
        draw_info[image_info[0]['file_name']] = ann_info
    return draw_info


def coco2vott(coco_info, img_root, cls_list, out_jsn):
    out_dict = {'frames':{}}
    cnt = 0
    for img_name, info in coco_info.items():
        img_fp = os.path.join(img_root, img_name)
        height, width, _ = cv2.imread(img_fp).shape
        out_dict['frames'][img_name] = list()
        for bbox_info in info:
            label = cls_list[bbox_info['category_id']-1]
            bbox = bbox_info['bbox']
            xmin, ymin, xmax, ymax = int(bbox[0]), int(bbox[1]), int(bbox[0] + bbox[2]), int(bbox[1] + bbox[3])
            bbox_info_dict = dict(x1=xmin, y1=ymin, x2=xmax, y2=ymax)
            bbox_info_dict['tags'] = [label] 
            bbox_info_dict['type'] = 'rect'

            bbox_info_dict['height'] = height
            bbox_info_dict['width'] = width
            bbox_info_dict['id'] = cnt 
            bbox_info_dict['name'] = img_name 

            bbox_info_dict['points'] = []
            bbox_info_dict['points'].append({'x': xmin, 'y': ymin})
            bbox_info_dict['points'].append({'x': xmin, 'y': ymax})
            bbox_info_dict['points'].append({'x': xmax, 'y': ymax})
            bbox_info_dict['points'].append({'x': xmax, 'y': ymin})

            bbox_info_dict['box'] = {}
            bbox_info_dict['box']['x1'] = xmin
            bbox_info_dict['box']['y1'] = ymin
            bbox_info_dict['box']['x2'] = xmax
            bbox_info_dict['box']['y2'] = ymax


            out_dict['frames'][img_name].append(bbox_info_dict)
            cnt += 1

    with open(out_jsn, 'w') as out:
        json.dump(out_dict, out, indent=2)
        
            

if __name__ == '__main__':
    coco_annotation = '../../../../data/COCO/annotations/instances_val2017_subset.json'
    img_root = '../../../../data/COCO/val2017_subset'
    cls_list = ['person', 'nonvehicle', 'vehicle', 'face']
    out_jsn = 'val2017_subset.json'

    coco_info = decode_coco(coco_annotation)
    coco2vott(coco_info, img_root, cls_list, out_jsn)
